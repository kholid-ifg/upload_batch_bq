const { BigQuery } = require('@google-cloud/bigquery');
const cron = require('node-cron');
const nodemailer = require('nodemailer');
require('dotenv').config()
var fs = require('fs');
const { table } = require('console');
let del = false


// Replace with the path to your service account JSON key file
//const keyFilename = `./service-account/${process.env.SERVICE_ACCOUNT}`;
const keyFilename = `master-gcp-bq.json`;


// Initialize BigQuery client
const bigquery = new BigQuery({ keyFilename });
var obj = JSON.parse(fs.readFileSync(keyFilename, 'utf8'));


// Specify the dataset and table to load data into
const datasetId = 'IFRS17'
const tableId = `MARKET_DATA`


// Define the job configuration
const jobConfig = {
  sourceFormat: 'CSV',
  skipLeadingRows: 1, // Skip the header row if present in CSV files
};

const query = "INSERT INTO "+datasetId+"."+tableId+" (QUOTE_DT, QUOTE_ID, MATURITY_DT, QUOTE_RT) VALUES('2024-04-10', 'TESSS', '2024-04-10', NULL)";

// Options for the query job
const options = {
  query: query,
  // Location is optional but recommended
  //location: 'US',
};

 async function runQuery() {
  try {
    const [job] = await bigquery.createQueryJob(options);

    // Wait for the query job to complete
    const [rows] = await job.getQueryResults();

    // Process the results
    rows.forEach(row => {
      console.log(row);
    });

    console.log('Query completed successfully.');
  } catch (err) {
    console.error('Error running query:', err);
  }
}

// Create a job to load data into BigQuery
async function loadToDataQuotes() {

  //Local file 
//  const filename = 'template_quotes.csv';
 const filename = 'isi nama file csv, masukkan ke folder project';

// const tableId = `${process.env.QUOTE}`;
const tableId = `isikan tableID/nama table`;

const startTime = Date.now();



await bigquery
    .dataset(datasetId)
    .table(tableId)
    .load(`./data-market/`+filename, jobConfig)
    //.load(filename, metadata)
    .then((results) => {
      const job = results[0];

      fs.copyFile('./data-market/'+filename, './data-market-bc/'+filename+'-'+new Date().toJSON().slice(0, 10)+'-'+new Date().toJSON().slice(11, 19).replace(/:/g,""),function (err) {
        if (err) console.log(err);
       })


      // Calculate the duration
      const endTime = Date.now();
      const duration = endTime - startTime;

      // Job completed successfully
      console.log(`Job upload Quotes ${job.id} completed in ${duration} ms.`);
        setTimeout(() => {
          deleteFile(filename)
        }, "3000");
  }).catch((err)=>{
    console.log(err)
  })
 

}

// async function loadToDataPDLGD() {
// const tableId = `${process.env.PG_LGD}`
// //Local file 
// const filename = 'template_pd_lgd.csv';

// const startTime = Date.now();



// await bigquery
//     .dataset(datasetId)
//     .table(tableId)
//     .load('./data-market/'+filename, jobConfig)
//     //.load(filename, metadata)
//     .then((results) => {
//       fs.copyFile('./data-market/'+filename, './data-market-bc/'+filename+'-'+new Date().toJSON().slice(0, 10)+'-'+new Date().toJSON().slice(11, 19).replace(/:/g,""),function (err) {
//         if(err){
//           console.log(err);
//         }
//        })

//       const job = results[0];

//       // Calculate the duration
//       const endTime = Date.now();
//       const duration = endTime - startTime;

//       // Job completed successfully
//       console.log(`Job upload PG_LGD ${job.id} completed in ${duration} ms.`);

      
//       setTimeout(() => {
//         deleteFile(filename)
//       }, "3000");

//   }).catch((err)=>{
//     console.log(err)
//   })
 
 
// }

// async function loadToDataRiskFactor() {
// const tableId = `${process.env.RISK_FACTOR}`;
// //Local file 
// const filename = 'template_risk_factor.csv';


// const query = `TRUNCATE TABLE PSAK74.${tableId};`;

// // For all options, see https://cloud.google.com/bigquery/docs/reference/rest/v2/jobs/query
// const options = {
//   query: query,
// };

// // Run the query as a job
// const [job] = await bigquery.createQueryJob(options);
// console.log(`Truncate success with job id ${job.id}.`);


// const startTime = Date.now();

// await bigquery
//     .dataset(datasetId)
//     .table(tableId)
//     .load('./data-market/'+filename, jobConfig)
//     //.load(filename, metadata)
//     .then((results) => {
//       const job = results[0];
//       fs.copyFile('./data-market/'+filename, './data-market-bc/'+filename+'-'+new Date().toJSON().slice(0, 10)+'-'+new Date().toJSON().slice(11, 19).replace(/:/g,""),function (err) {
//         if (err) console.log(err);
//        })
//       // Calculate the duration
//       const endTime = Date.now();
//       const duration = endTime - startTime;

//       // Job completed successfully
//       console.log(`Job ${job.id} completed in ${duration} ms.`);
//       setTimeout(() => {
//         deleteFile(filename)
//       }, "3000");
//   }).catch((err)=>{
//     console.log(err)
//   })
 
 
// }

// async function loadToDataRiskFactorCurve() {
// const tableId = `${process.env.RISK_FACTOR_CURVE}`

// //Local file 
// const filename = 'template_risk_factor_curve.csv';

// const query = `TRUNCATE TABLE PSAK74.${tableId};`;

// // For all options, see https://cloud.google.com/bigquery/docs/reference/rest/v2/jobs/query
// const options = {
//   query: query,
// };

// // Run the query as a job
// const [job] = await bigquery.createQueryJob(options);
// console.log(`Truncate success with job id ${job.id}.`);


// const startTime = Date.now();




// await bigquery
//     .dataset(datasetId)
//     .table(tableId)
//     .load(filename, jobConfig)
//     //.load(filename, metadata)
//     .then((results) => {
//       const job = results[0];
//       fs.copyFile('./data-market/'+filename, './data-market-bc/'+filename+'-'+new Date().toJSON().slice(0, 10)+'-'+new Date().toJSON().slice(11, 19).replace(/:/g,""),function (err) {
//         if (err) console.log(err);
//        })
    
//       // Calculate the duration
//       const endTime = Date.now();
//       const duration = endTime - startTime;

//       // Job completed successfully
//       console.log(`Job ${job.id} completed in ${duration} ms.`);
//       setTimeout(() => {
//         deleteFile(filename)
//       }, "3000");
//   }).catch((err)=>{
//     console.log(err)
//   })
 
// }
// async function loadToDataRiskFactorXRiskFactorCurve() {
//   const tableId = `${process.env.RISK_FACTOR_CURVE_X_RISK_FACTOR}`
//   const query = `TRUNCATE TABLE PSAK74.${tableId};`;
//   const filename = 'template_risk_factor_x_risk_factor_curve-v1'

//   // For all options, see https://cloud.google.com/bigquery/docs/reference/rest/v2/jobs/query
//   const options = {
//     query: query,
//   };
  
//   // Run the query as a job
//   const [job] = await bigquery.createQueryJob(options);
//   console.log(`Truncate success with job id ${job.id}.`);

// const startTime = Date.now();
// await bigquery
//     .dataset(datasetId)
//     .table(tableId)
//     .load(filename, jobConfig)
//     //.load(filename, metadata)
//     .then((results) => {
//       fs.copyFile('./data-market/'+filename, './data-market-bc/'+filename+'-'+new Date().toJSON().slice(0, 10)+'-'+new Date().toJSON().slice(11, 19).replace(/:/g,""),function (err) {
//         if (err) console.log(err);
//        })
//       const job = results[0];

//       // Calculate the duration
//       const endTime = Date.now();
//       const duration = endTime - startTime;

//       // Job completed successfully
//       console.log(`Job ${job.id} completed in ${duration} ms.`);
//       setTimeout(() => {
//         deleteFile(filename)
//       }, "3000");
//   }).catch((err)=>{
//     console.log(err)
//   })

// }


// function deleteFile(nameFile){
// fs.unlinkSync('./data-market/'+nameFile)
// return true 
// }

// function sendEmail(){
// const transporter = nodemailer.createTransport({
//   host: 'smtp.office365.com',
//   port: 587,
//   auth: {
//     user: `${process.env.SMTP_EMAIL}`,
//     pass: `${process.env.PASS_SMTP}`,
//   },
// });
//  const mailOptions = {
//   from: 'notification@ifg.id',
//   to: 'edwin.penangsang@ifg.id,iota.laseria@ifg.id',
//   subject: 'Notification Data Market',
//   text: 'Data market has been send to BigQuery with shceduller. '+new Date(),
// };

// setTimeout(async () => {
//   await transporter.sendMail(mailOptions);
// }, "1000");
// }

runQuery()

// cron.schedule('1 * * * * *', function() {
//   loadToDataQuotes()
//   loadToDataPDLGD()
//   loadToDataRiskFactor()
//   loadToDataRiskFactorCurve()
//   loadToDataRiskFactorXRiskFactorCurve()
//   sendEmail()
//   console.log('Running task on', new Date());
// });


