const { BigQuery } = require('@google-cloud/bigquery');
const cron = require('node-cron');
const nodemailer = require('nodemailer');
require('dotenv').config()
var fs = require('fs');
const { table } = require('console');
let del = false


// Replace with the path to your service account JSON key file
//const keyFilename = `./service-account/${process.env.SERVICE_ACCOUNT}`;
const keyFilename = `master-gcp-bq.json`;


// Initialize BigQuery client
const bigquery = new BigQuery({ keyFilename });
var obj = JSON.parse(fs.readFileSync(keyFilename, 'utf8'));


// Specify the dataset and table to load data into
const datasetId = 'IFRS17'
const tableId = `MARKET_DATA`


// Define the job configuration
const jobConfig = {
  sourceFormat: 'CSV',
  skipLeadingRows: 1, // Skip the header row if present in CSV files
};

// const query = "INSERT INTO "+datasetId+"."+tableId+" (QUOTE_DT, QUOTE_ID, MATURITY_DT, QUOTE_RT) VALUES('2024-04-10', 'TESSS', '2024-04-10', NULL)";

// Options for the query job
const options = {
  // query: query,
  // Location is optional but recommended
  //location: 'US',
};

// Create a job to load data into BigQuery
async function loadToDataQuotes() {

  //Local file 
  //  const filename = 'template_quotes.csv';
  const filename = 'tes.csv';

  const startTime = Date.now();

  await bigquery
    .dataset(datasetId)
    .table(tableId)
    .load(filename, jobConfig)
    //.load(filename, metadata)
    .then((results) => {
      const job = results[0];

      // fs.copyFile('./data-market/'+filename, './data-market-bc/'+filename+'-'+new Date().toJSON().slice(0, 10)+'-'+new Date().toJSON().slice(11, 19).replace(/:/g,""),function (err) {
      //   if (err) console.log(err);
      //  })


      // Calculate the duration
      const endTime = Date.now();
      const duration = endTime - startTime;

      // Job completed successfully
      console.log(`Job upload Quotes ${job.id} completed in ${duration} ms.`);
        setTimeout(() => {
          // deleteFile(filename)
        }, "3000");
  }).catch((err)=>{
    console.log(err)
  })
 

}

// runQuery()
loadToDataQuotes();
// cron.schedule('1 * * * * *', function() {
//   loadToDataQuotes()
//   loadToDataPDLGD()
//   loadToDataRiskFactor()
//   loadToDataRiskFactorCurve()
//   loadToDataRiskFactorXRiskFactorCurve()
//   sendEmail()
//   console.log('Running task on', new Date());
// });


